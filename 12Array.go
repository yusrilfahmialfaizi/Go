package main

import "fmt"

func main() {
	// array
	var names [4]string

	names[0] = "Yusril"
	names[1] = "Fahmi"
	names[2] = "Al"
	names[3] = "Faizi"

	fmt.Print(names[0], " ", names[1], " ", names[2], " ", names[3])
	fmt.Println()

	var values = [3]int32 {
		10000,
		20000,
		30000,
	}
	fmt.Println(values)

	var val []string

	fmt.Println("length = ", len(values))
	fmt.Println("length = ", len(val))
}