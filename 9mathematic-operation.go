package main

import "fmt"

func main() {
	a := 10
	b := 30

	c := a + b
	d := a - b
	e := a * b
	f := a / b
	g := a % b

	fmt.Println(c)
	fmt.Println(d)
	fmt.Println(e)
	fmt.Println(f)
	fmt.Println(g)

	//augmented assignments
	h := 10

	h += 10 //h = h+10

	fmt.Println(h)

	// Unary Operator
	// ++, --, -, +, !

	h++ //h = h+1

	negative := -100
	positive := +100

	fmt.Println(h)
	fmt.Println(negative)
	fmt.Println(positive)
}