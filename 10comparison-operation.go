package main

import "fmt"

func main() {
	// Operasi perbandingan
	// >, <, >=, <=, ==, !=

	name1 := "Eko"
	name2 := "Eko"
	name3 := "Budi"

	result1 := name1 == name2
	result2 := name1 == name3

	fmt.Println(result1)
	fmt.Println(result2)
}