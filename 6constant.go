package main

import "fmt"

func main() {
	// variable yang nilainya tidak bisa di ubah lagi setelah diberi nilai
	// const boleh jika tidak langsung dipakai
	const firstName string = "Yusril"

	fmt.Println(firstName)

	// Deklarasi multiple const
	const(
		lastName = "Fahmi"
		age = 35
		city = "Jember"
	)

	fmt.Println(firstName, lastName)

}