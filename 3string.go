package main

import "fmt"

func main() {
	fmt.Println("Yusril Fahmi Al Faizi")

	fmt.Println(len("Yusril Fahmi Al Faizi")) //get length of string
	fmt.Println("Yusril Fahmi Al Faizi"[0]) // get index from string with byte output
}
