package main

import "fmt"

func main() {
	fmt.Println("Variable : ")
	// variable di go bersifat unique 
	//dan tidak dapat dipakai di program yang sama 
	//dengan tipe data yang berbeda

	var name string

	name = "Yusril Fahmi Al Faizi"

	fmt.Println(name)

	name = "Al Faizi"

	fmt.Println(name)

	var city = "Jember"

	fmt.Println(city)

	var age = 23

	fmt.Println(age)

	// membuat variable tanpa menggunakan var hanya untuk deklarasi awal
	// menggunakan :=

	date := "24 September 1998"

	fmt.Println(date)

	// Declare multivariable
	var (
		firstName = "Yusril Fahmi"
		lastName = "Al Faizi"
	)

	fmt.Print(firstName, lastName)

}