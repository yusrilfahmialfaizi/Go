package main

import "fmt"

func main() {
	// Slice atau potongan data array
	// Slice -> pointer(acuan data pertama), length, capacity
	var months = [...]string{
		"Januari",
		"Februari",
		"Maret",
		"April",
		"Mei",
		"Juni",
		"Juli",
		"Agustus",
		"September",
		"Oktober",
		"November",
		"Desember",
	}

	var slice1 = months[4:7]

	fmt.Println(slice1)
	fmt.Println("Length : ", len(slice1))
	fmt.Println("Capacity : ", cap(slice1))
	// append : menambah data baru di paling terakhir, jika sudah memnuhi capacity maka akan membuat slice baru
	// make : digunakan untuk membuat slice baru
	// copy : menyalin slice dari source ke destination

	var days = [...]string {
		"Senin", "Selasa", "Rabu", "Kamis", "Jum'at", "Sabtu", "Minggu",
	}
	
	day1 := days[0:4]

	day2 := append(day1, "Minggu Libur")

	fmt.Println(day2)

	newSlice := make([]string, 2, 5)

	newSlice[0] = "Yusril"
	newSlice[1] = "Fahmi"

	fmt.Println(newSlice)
	
	copySlice := make([]string, len(newSlice), cap(newSlice))
	copy(copySlice, newSlice)

	fmt.Println(copySlice)
}