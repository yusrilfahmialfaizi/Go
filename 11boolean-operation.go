package main

import "fmt"

func main() {
	// &&, ||, !
	a := true
	b := false

	c := a && b
	d := a || b
	e := !a
	f := !b

	fmt.Println(c)
	fmt.Println(d)
	fmt.Println(e)
	fmt.Println(f)
}