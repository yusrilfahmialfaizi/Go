package main

import "fmt"

func main() {
	// Konversi tipe data misal dari int32 ke 66

	var nilai32 int32 = 100000
	var nilai64 int64 = int64(nilai32)
	// konversi nilai32 ke int8 bisa dilakukan tetapi harus memperhatikan range dari nilai32
	var nilai8 int8 = int8(nilai32)

	fmt.Println(nilai32)
	fmt.Println(nilai64)
	fmt.Println(nilai8)

	// konversi tipe data dari byte ke string kembali
	name := "Yusril"
	y := name[0] //output byte

	yString := string(y)
	fmt.Println(yString)

}