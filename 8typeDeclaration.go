package main

import "fmt"

func main() {
	// Aliasing

	type NoKTP string // string alias NoKTP
	type status bool

	var ktp NoKTP = "12321313131"
	var statusNikah status = true
	
	fmt.Println(ktp)
	fmt.Println(statusNikah)
}