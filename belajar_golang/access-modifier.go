package main

// kata kunci yang menentukan adalah huruf di awal variable atau function

import "fmt"

var version int  = 1 //tidak bisa di akses dari luar package

var Version int  = 2 //bisa di access dari luar package

// tidak bisa di akses dari luar pacakage ini
func sayHello()  {
	fmt.Println()
}

// bisa di akses dari luar package
func SayHello(){
	fmt.Println()
}